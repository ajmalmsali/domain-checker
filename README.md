DomainSearch - Domain Availability search
-------
Helps you to check for available domains in a range

Built with Sinatra and Whois Gem

Use http://site-root.com/search/start-range/end-range

DEMO at http://afternoon-thicket-5373.herokuapp.com/search/start-range/end-range {replace start-range and end-range}

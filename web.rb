require 'sinatra'
require 'whois'

class AvailableDomains
	def initialize()
		@dom_arr=[]
		@job_id=Time.now.to_s
	end
	def job(startr,endr)
		
		aFile = File.new("/app/tmp/jobs/job_"+@job_id, "w")

		(startr..endr).each { |x|

		dom=x.to_s+".com";
		r = Whois.whois(dom)
		aFile.puts(dom) if r.available?

		}
		aFile.close
		return "job_id:"+@job_id
	end

	def list_jobs
		#return "OK"
		#a=Dir.chdir("/app/tmp/jobs/")
		#a.to_s
		ar=Dir.entries("/app/tmp/")
		#Dir.pwd
		ar.inspect
	end

	def job_results(job_id)
		Dir.chdir("/app/tmp/jobs/")
		arr=IO.readlines("job_"+job_id)
		arr.join
	end
end

get '/search/:start_r/:end_r' do
	ar=AvailableDomains.new
	ar.job(params[:start_r],params[:end_r])
end

get '/results/:job_id/' do

	ar=AvailableDomains.new
	ar.job_results(params[:job_id])
end

get '/jobs/' do
	ar=AvailableDomains.new
	ar.list_jobs
end